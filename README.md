# Summer-projekt 2017: Coding skill-check after university #
A number of programming languages, tools and theories are applied here to update myself and future prospective employers on coding and systems design.  
  
## What is this repository for? ##
* A multi-part project which fulfills a set goal: A simple assessment tool and platform for recording and evaluating how much time given business operations take to complete, based on mouse movement statistics and different data.  
* Current goal: Skeleton - Conceptual design and physical skeleton example functionality with desktop app, SQL database and web-app.  
* Next goal: Proper data flows between desktop app, db and web-app.  
* Current final goal: Above two goals, together with final interface design: The V in MVC.  
* Future final goal 1: Established and clear platform to expand upon, with mobile support and a distributed SQL server.  
* Future final goal 2: Mouse support only leaves a ton of data unwatched, doesn't it?  

## Solution design ##
* Business process example: Clarity redefined.  
* Use case, because it's useable for someone.  
* Class diagram: Basically a MVC and publish-subscribe structure presented in a single picture. Manual and auto-generated designs.  
* Web design plan
* Database solution: ER-diagram or/and textual representation, depending on complexity.
* Prototypes (?), probably not.

## Solution presentation ##
### Screenshots ###
of everything.
### Sequence diagrams ###
#### Desktop application ####
* Set relevant business operation name (Before/after recording? After means guaranteed notice from user via pop-up (Name can be null, is set according to a guide, for example current time and date.)).
* Start business operation recording via GUI or keyboard shortcut.
* Stop business operation recording via GUI or keyboard shortcut.
* Clear all records (Custom time-set is a later feature).
* Export XML-report per business operation of #clicks, avg.time between clicks, total elapsed time (possible business time-save).

#### Web application ####
* Choose business operation from list.
* Clear selected record.

## Contact info ##
* Mikael R�dberg
* radberg.mikael@gmail.com
* 0706 84 06 95